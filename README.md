# Mini-Brief 03: Sensy
## Sense Something

Sense something. Use your Arduino to sense something in a place in Greater London. The system might sense light, sound, touch, movement, cars, dogs or wind, and show the data it is sensing through Serial output (saved to a file, possibly analysed for max/min/median) or for more advanced students, using another sensory mode such as an LED’s brightness. Create a short video (20-45 seconds) of this system in action and upload here and post to the forum.

## Brief

I make pour over coffee most days, and over the last few weeks, have become interesed in the whole sensation of the experience. Which made me thing this would be an interesting thing to try and sense, and record - and to then see what art could be made out of it.

Using sensors I had available, I've tried to record aspects of the whole experience.

## Project Setup

The project makes use of the following sensors

[Adafruit SPW2430 MEMS Microphone Breakout][1]

[Adafruit MAX9814 Electret Microphone Amplifier][2]

[Adafruit MCP9808 Precision Temerpature Sensor][3]

[Adafruit LIS3DH Tripe Axis Accelerometer][4]

Which were connected to the following

[Fellow Stagg Pour Over Kettle][5]

This had the LIS3DH acceleromter attached to the handle, correctly orientated so that the Z access was the up/down axis, and one of the MCP9808 temperature sensors attached to the body.

[Fellow Stagg Tasting Glasses][6]

This had the other MCP9808 sensor attached (this neeed to be at the very rim of thr glass, as the outside does not change temperature that much due to insulation). It also had the SPW2430 sensor attached, but I don't think this is functionng correctly, as it hovers around a base level value with incredibly little variation

## Data Collection

Coffee was made, and the results recorded via the serial connection - the output can be found [here][7] and was recorded using my iPhone using the time lapse function to compress about 5 minutes of coffee making into 20s. Video can be found [here][8]

## Refelections

Shame the SPW2430 sesnor is not behaving as expected, was hoping to get sound of coffee dripping into the glass - may need to try a new one.

Would have liked to record data to the onboard flash on the metro express, be good to get head round this.

A few extra sensors would have been good.

## Photos

![sensors](./photos/sensors.png)
![board](./photos/boardBuild.png)
![assembled](./photos/assmbledProject.png)
![filming](./photos/filmSetup.png)







[1]:https://www.adafruit.com/product/2716
[2]:https://www.adafruit.com/product/1713
[3]:https://www.adafruit.com/product/1782
[4]:https://www.adafruit.com/product/2809 

[5]:https://fellowproducts.com/products/stagg/
[6]:https://fellowproducts.com/collections/serve/products/stagg-tasting-glasses-pair

[7]:output/run01.csv
[8]:https://vimeo.com/367614649