#include <Wire.h>
#include "Adafruit_MCP9808.h"
#include <Adafruit_LIS3DH.h>
#include <Adafruit_Sensor.h>


//Sensors
//Board
//MAX9481 - Electret Microphone with Auto Gain Control
//Kettle
//LIS3DH - 3 dof accelerometer
//MCP9808 - Temperature Sensor
//Coffee Cup
//SPW430 - MEMS microphone - does not appear to be working correctly
//MCP9808 - Temperature Sensor

//Analogue in pins
#define SPW430_DC_PIN A0
#define MAX9841_PIN A1          

//I2C Addresses
//  LIS3DH          0x18 - default - 3 axis accelerometer
//  MCP9808-A       0x19 - A0 tied to VDD - Temperature A
//  MCP9808-B       0X1A - A1 toed to VDD - Temperature B

//Sample Measurements

#define MAX9841_SAMPLE_WINDOW 50
#define MAX9841_SAMPLE_MIN 0
#define MAX9841_SAMPLE_MAX 1024
#define MAX9481_UPPER_LIMIT 80             //Determined by experiment

#define LIS3DH_RAW_MIN -32768
#define LIS3DH_RAW_MAX 32767

unsigned int max9841;
float max9841_smoothed;
float max9841_smoothed2;

float smoothX;
float smoothY;
float smoothZ;

//Temperature Sensors
Adafruit_MCP9808 tempSensorA = Adafruit_MCP9808();
Adafruit_MCP9808 tempSensorB = Adafruit_MCP9808();

//3 Axis Accelerometer
Adafruit_LIS3DH accelSensor = Adafruit_LIS3DH();        //listen on I2C (no need to define pins unless we use SPI)

void setup() {

  //Set up serial interface for output
  Serial.begin(9600);
  //Wait for the serial interface before doing anything else
  while(!Serial) {
  }
  //Setup temp sensors
  if (!tempSensorA.begin(0x19)) {
    Serial.println("couldn't find tempSensorA");
  }
  if (!tempSensorB.begin(0x1A)) {
    Serial.println("couldn't find tempSensorB");
  }
  tempSensorA.setResolution(1);           //set a fast, low resolution - 0.25ºC @ 65ms
  tempSensorB.setResolution(1);           //set a fast, low resolution - 0.25ºC @ 65ms

  tempSensorA.wake();
  tempSensorB.wake();
  //Setup accelerometer
  if (!accelSensor.begin(0x18)) {
    Serial.println("couldn't find accelometer");
  }

  accelSensor.setRange(LIS3DH_RANGE_2_G);   // set to the lowest range

  //Setup microphones
  pinMode(SPW430_DC_PIN, INPUT);
  pinMode(MAX9841_PIN, INPUT);

  //Initialise exponentiall smoothed value for the 9481 microphone
  max9841_smoothed = 0.0;

  //titles for CSV / Serial plotter
  Serial.println("X\tY\tZ\tsmoothX\tsmoothY\tsmoothZ\ttempA\ttempB\tslContact\tslSmoothed");

}



void loop() {

  //Use a 50ms (should measure ƒ as low as 20Hz) sample window for the electret microphone so we can get a sound level
  //code based on https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels

  unsigned long  sampleStart = millis();      //get start of the sample time
  unsigned int peakToPeak = 0;                //peak to peak level which is what we will record

  unsigned int signalMax  =  MAX9841_SAMPLE_MIN;               //start at opposite end, so we always record new maxs
  unsigned int signalMin =  MAX9841_SAMPLE_MAX;             //start at opposite end, so we always record new mins

  while (millis() -  sampleStart < MAX9841_SAMPLE_WINDOW) {
    max9841 =  analogRead(MAX9841_PIN);
    if (max9841 < MAX9841_SAMPLE_MAX) {
      //only use samples within range
      if (max9841 > signalMax) {
        signalMax = max9841;
      } else {
        signalMin = max9841;
      }
    }
  }
  peakToPeak = signalMax - signalMin;
  //Exponential smoothing and normalistion
  max9841_smoothed = floatMap(exponentialSmoothing(max9841_smoothed, peakToPeak, 0.1), 0, MAX9481_UPPER_LIMIT, 0, 1, true);

  //read temerpature sensors and map to 0 > 1 range
  float tempA = floatMap(tempSensorA.readTempC(), 0, 100, 0, 1, true);
  float tempB = floatMap(tempSensorB.readTempC(), 0, 100, 0, 1, true);

  accelSensor.read();       // read the acceleomter

  int micA_DC = analogRead(SPW430_DC_PIN);

  //get accelerometer data and normailse to -1 > 1 range
  float x = floatMap(accelSensor.x, LIS3DH_RAW_MIN, LIS3DH_RAW_MAX, -1.0, 1.0, false);
  float y = floatMap(accelSensor.y, LIS3DH_RAW_MIN, LIS3DH_RAW_MAX, -1.0, 1.0, false);
  float z = floatMap(accelSensor.z, LIS3DH_RAW_MIN, LIS3DH_RAW_MAX, -1.0, 1.0, false);

  //Smoothed accelerometer data
  smoothX = exponentialSmoothing(smoothX, x, 0.7);
  smoothY = exponentialSmoothing(smoothY, x, 0.7);
  smoothZ = exponentialSmoothing(smoothZ, x, 0.7);
  
  //Dump all the results to serail
  Serial.print(x);
  Serial.print("\t");
  Serial.print(y);
  Serial.print("\t");
  Serial.print(z);
  Serial.print("\t");
  Serial.print(smoothX);
  Serial.print("\t");
  Serial.print(smoothY);
  Serial.print("\t");
  Serial.print(smoothZ);
  Serial.print("\t");
  Serial.print(tempA);
  Serial.print("\t");
  Serial.print(tempB);
  Serial.print("\t");
  Serial.print(floatMap(micA_DC,0,1000,0,1,false));
  Serial.print("\t");
  Serial.println(max9841_smoothed);

  delay(1);




}

float exponentialSmoothing(float average, float sample, float alpha) {
  //Exponential smoothing - a slightly easier to compute average than a moving average, as we only need the current measurement, and the last average
  return ( alpha * sample ) + (( 1.0 - alpha) *  average);
}


float floatMap(float value, float inputMin, float inputMax, float outputMin, float outputMax, bool clamp) {
  //Native arduino map uses interger math - so not suitable for try to normalise values in a 0 > 1, or -1 > 1 range (as will return 0 or 1!)
  //so this is the float version of this instead
  float r = (value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin) + outputMin;
  if (clamp && r < outputMin) {
    r = outputMin;
  }
  if (clamp && r > outputMax) {
    r = outputMax;
  }
  return r;
}
